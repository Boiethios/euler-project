{- Multiples of 3 and 5

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

-}

import qualified Data.List as List

result :: [Int] -> Int
result =
  List.sum . List.filter (\x -> x `rem` 3 == 0 || x `rem` 5 == 0)

main :: IO()
main =
  print $ result [1..999]