{- Special Pythagorean triplet

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a² + b² = c²

For example, 3² + 4² = 9 + 16 = 25 = 5².

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

-}

import qualified Data.List as List
import qualified Control.Exception.assert as assert

pythagorean_triplets :: [(Int, Int, Int)]
pythagorean_triplets =
  [ (a, b, c)
    | m <- [1..]
    , n <- [1..m]
    , let a = m*m - n*n
    , let b = 2*m*n
    , let c = m*m + n*n
    ]

apply :: (Int -> Int -> Int) -> (Int, Int, Int) -> Int
apply f (a, b, c) =
  f c $ f a b

result :: Maybe Int
result =
  fmap (apply (*)) found
  where
  found = List.find (\t -> (==) 1000 $ apply (+) t) pythagorean_triplets

main :: IO()
main =
  assert (result == Just 31875000) (return ())
