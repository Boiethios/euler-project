{- Smallest multiple

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

-}

import qualified Data.List as List

result :: [Int] -> Int
result =
  List.foldr1 lcm

main :: IO()
main =
  print $ result [1..20]
