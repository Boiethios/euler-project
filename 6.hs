{- Sum square difference

The sum of the squares of the first ten natural numbers is,
1² + 2² + ... + 10² = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)² = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

-}

import qualified Data.List as List

result :: Int -> Int
result max =
  let
    sum1 = List.sum [ n * n | n <- [1..max] ]
    sum2 = List.sum [1..max] ^ 2
  in
    sum2 - sum1

main :: IO()
main =
  print $ result 100
