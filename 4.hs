{- Largest palindrome product

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

-}

import qualified Data.List as List

isPalindromic :: Int -> Bool
isPalindromic n =
  (reverse . show) n == show n

result :: Int -> Int
result up =
  maximum [ n
    | i <- [1..up]
    , j <- [1..up]
    , let n = i * j
    , isPalindromic n]

main :: IO()
main =
  print $ result 999
